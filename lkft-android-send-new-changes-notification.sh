#!/bin/bash -ex

apt -qy update
apt -qy install jq unzip sudo git make

##########functions definition########################
# need to be in a file that could be sourced
# but need to download with curl or wget is not good
# so keep a copy here from the same one in lkft-android-call-jenkins.sh
function send_mail_common(){
    local f_email_body="${1}"
    local email_subject="${2}"

    wget -c https://gitlab.com/Linaro/lkft/pipelines/common/raw/master/build-email.py -O build-email.py
    wget -c https://gitlab.com/Linaro/lkft/pipelines/common/raw/master/send-email.py -O send-email.py
    pip --no-cache-dir --quiet install boto3

    python build-email.py \
        --msg-from=${REVIEW_FROM} \
        --msg-to=${REVIEW_TO} \
        --msg-cc=${REVIEW_CC} \
        --msg-body=${f_email_body} \
        --msg-subject="${email_subject}" \
        "--msg-body-pre=Go here to check the pipeline jobs: ${CI_PIPELINE_URL}." >mail.txt

    python send-email.py mail.txt
}
######################################################

kernel_version=$(make kernelversion)
kernel_branch="${CI_BUILD_REF_NAME}"
kernel_label="${kernel_version}-${CI_COMMIT_SHA:0:12}"

f_email="notification-new-changes.txt"
url_repository="${KERNEL_REPO}/-/tree/${CI_BUILD_REF_NAME}/"

rm -fr ${f_email}
echo "Please check the following for the changes information:" > ${f_email}
# https://gitlab.com/Linaro/lkft/users/yongqin.liu/android-common/-/tree/android-mainline
echo "Repository Link: ${url_repository}" >> "${f_email}"
echo "CI_COMMIT_MESSAGE: ${CI_COMMIT_MESSAGE}" >> "${f_email}"

msg_subject="[lkft-android-report] ${CI_PROJECT_NAME} | ${kernel_branch} | ${kernel_label}"

send_mail_common "${f_email}" "${msg_subject}"

## record the kernel version information for the later stage jobs
## which will be in the artifacts
f_kernel_version_json="kernel-version.json"
echo "{" > "${f_kernel_version_json}"
echo "    \"KERNEL_REPOSITORY\": \"${url_repository}\","  >> "${f_kernel_version_json}"
echo "    \"KERNEL_BRANCH\": \"${kernel_branch}\","  >> "${f_kernel_version_json}"
echo "    \"SRCREV_kernel\": \"${CI_COMMIT_SHA}\","  >> "${f_kernel_version_json}"
echo "    \"KERNEL_DESCRIBE\": \"${kernel_label}\","  >> "${f_kernel_version_json}"
echo "    \"MAKE_KERNELVERSION\": \"${kernel_version}\""  >> "${f_kernel_version_json}"
echo "}" >> "${f_kernel_version_json}"
