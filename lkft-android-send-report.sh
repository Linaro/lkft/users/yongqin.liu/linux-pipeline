#!/bin/bash -ex

kernel_branch="${1}"
pwd
ls -l $(pwd)
env
echo "why no build-email.py"

apt -qy update
apt -qy install jq unzip sudo git
pip --no-cache-dir --quiet install boto3

if [ -f Makefile ]; then
    kernel_version=$(make kernelversion)
elif [ -f build.json ]; then
    kernel_version=$(jq -r '.[] | .kernel_version' build.json)
else
    kernel_version="unkonwn"
fi
kernel_label="${kernel_version}-${CI_COMMIT_SHA:0:12}"

wget -c https://raw.githubusercontent.com/Linaro/android-report/master/deploy.sh -O deploy.sh && chmod +x deploy.sh

mkdir -p /android/django_instances
./deploy.sh /android/django_instances false
f_report="/tmp/kernelreport-${kernel_branch}-${kernel_label}.txt"
for i in {1..5}; do
    echo "Try for the $i times to generate the report"
    /android/django_instances/android-report/kernelreport.sh "${kernel_branch}" --exact-version-1 "${kernel_label}" --no-check-kernel-version
    if [ -f "${f_report}" ]; then
        break
    fi
done

if [ -f "${f_report}" ]; then
    cp -vf "${f_report}" report.txt
else
    echo "Tried to generate the report for kernel: ${kernel_branch} ${kernel_label} but failed" > report.txt
fi

wget -c https://gitlab.com/Linaro/lkft/pipelines/common/raw/master/build-email.py -O build-email.py
wget -c https://gitlab.com/Linaro/lkft/pipelines/common/raw/master/send-email.py -O send-email.py

msg_subject="[lkft-android-report] ${CI_PROJECT_NAME} | ${kernel_branch} | ${kernel_label}"
# https://gitlab.com/Linaro/lkft/pipelines/common/raw/master/build-email.py
python build-email.py \
    --msg-from=${REVIEW_FROM} \
    --msg-to=${REVIEW_TO} \
    --msg-cc=${REVIEW_CC} \
    "--msg-subject=${msg_subject}" \
    --msg-body=report.txt \
    "--msg-body-pre=Go here to regenerate the report: ${CI_PIPELINE_URL}." >review.txt

# https://gitlab.com/Linaro/lkft/pipelines/common/raw/master/send-email.py
python send-email.py review.txt
