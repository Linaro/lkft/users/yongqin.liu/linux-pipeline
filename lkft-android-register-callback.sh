#!/bin/bash -ex

env 

#export QA_PROJECT="${QA_PROJECT//\//_}"
lkft_build_config="${1}"
url_lkft_build_config="https://android-git.linaro.org/android-build-configs.git/plain/lkft/${lkft_build_config}?h=lkft"

rm -fr ${lkft_build_config}
wget -c ${url_lkft_build_config} -O ${lkft_build_config}
source ${lkft_build_config}

# TODO: has problem for multiple projects case
QA_PROJECT_GROUP="${TEST_QA_SERVER_TEAM}"
QA_PROJECT_NAME="${TEST_QA_SERVER_PROJECT}"

# TODO need to be dynamic for different projects
report_job_name="callback-for-squad-${lkft_build_config}"

# Find report job
echo "Will do tests. Need to register QA callback."
# https://docs.gitlab.com/ee/api/jobs.html
# when all the callback jobs finished, there will be no jobs returned with "scope[]=manual" specified
# scope is the status, will be changed when the job is running or finished, etc
# here does not support the scope to have all jobs returned, and the callback
# could be registered again, even it's called before
curl -f --silent "https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/pipelines/${CI_PIPELINE_ID}/jobs" > jobs-manual.json
job_id=$(jq -r ".[] | select(.name == \"${report_job_name}\") | .id" jobs-manual.json)
callback_url="https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/jobs/${job_id}/play"
echo "Callback URL: [${callback_url}]"

# Registr callback with SQUAD
f_build_json="build.json"
make_kernel_version="$(jq -r '.[0].kernel_version' ${f_build_json})"
kernel_commit="$(jq -r '.[0].git_sha' ${f_build_json})"
qa_build_version="${make_kernel_version}-${kernel_commit:0:12}"
project_fullname="${QA_PROJECT_GROUP}/${QA_PROJECT_NAME}"
query_qabuild_url="https://qa-reports.linaro.org/api/builds/?version=${qa_build_version}&project__slug=${QA_PROJECT_NAME}&project__full_name=${project_fullname}"

curl -f --silent -L "${query_qabuild_url}" -o qa_build.json
build_id="$(jq -r '.results[0].id' qa_build.json)"
curl --silent \
  -X POST "${QA_SERVER}/api/builds/${build_id}/callbacks/" \
  -H "Authorization: Token ${QA_REPORTS_TOKEN}" \
  -F "callback_url=${callback_url}" \
  -F "callback_record_response=true"
