#!/bin/bash -ex

ls -l .

F_ABS_PATH=$(readlink -e $0)
DIR_PARENT=$(dirname ${F_ABS_PATH})

ANDROID_BUILD_CONFIG="${1}"
if [ -z "${ANDROID_BUILD_CONFIG}" ]; then
    echo "No build config specified!"
    exit 1
fi

BUILD_TYPE="${2}"
if [ -z "${BUILD_URL}" ]; then
    BUILD_TYPE="tuxsuite"
fi

if [ "X${BUILD_TYPE}" = "tuxsuite" ]; then
    f_build_json="build.json"
elif [ "X${BUILD_TYPE}" = "jenkins" ]; then
    f_build_json="${ANDROID_BUILD_CONFIG}-build.json"
else
    echo "Unsported BUILD_TYPE specified: ${BUILD_TYPE}"
    exit 1
fi

if [ ! -f "${f_build_json}" ]; then
    echo "The ${f_build_json} does not exist, and we could not found the url to download kernel files"
    exit 1
fi


if [ "X${BUILD_TYPE}" = "tuxsuite" ]; then
    TUXSUITE_DOWNLOAD_URL="$(jq -r '.[] | .download_url' "${f_build_json}")"
    if [ -z "${TUXSUITE_DOWNLOAD_URL}" ]; then
        echo "Failed to get the value for TUXSUITE_DOWNLOAD_URL from the file of ${f_build_json}."
        exit 1
    fi

    GIT_DESCRIBE="$(jq -r '.[] | .git_describe' "${f_build_json}")"
    SRCREV_kernel="$(jq -r '.[] | .git_sha' "${f_build_json}")"
    KERNEL_VERSION="$(jq -r '.[] | .kernel_version' "${f_build_json}")"
    KERNEL_DESCRIBE=${KERNEL_VERSION}-${SRCREV_kernel:0:12}
    JOB_NAME="${CI_PROJECT_NAME}-${CI_COMMIT_REF_NAME}"
    BUILD_NUMBER="${CI_PIPELINE_ID}"
    BUILD_URL="${CI_PIPELINE_URL}"

elif [ "X${BUILD_TYPE}" = "jenkins" ]; then
    snapshot_download_url=""
    if [ -f "${f_gki_build_json}" ]; then
        snapshot_download_url=$(jq .SNAPSHOT_DOWNLOAD_URL "${f_gki_build_json}" |tr -d \")
    fi
    GIT_DESCRIBE=$(jq .KERNEL_DESCRIBE "${f_gki_build_json}" |tr -d \")
    SRCREV_kernel=$(jq .SRCREV_kernel "${f_gki_build_json}" |tr -d \")
    KERNEL_VERSION=$(jq .MAKE_KERNELVERSION "${f_gki_build_json}" |tr -d \")
    KERNEL_DESCRIBE=$(jq .KERNEL_DESCRIBE "${f_gki_build_json}" |tr -d \")
    JOB_NAME=$(jq .JENKINS_CI_BUILD_NAME "${f_gki_build_json}" |tr -d \")
    BUILD_NUMBER=$(jq .JENKINS_BUILD_NUMBER "${f_gki_build_json}" |tr -d \")
    BUILD_URL=$(jq .JENKINS_BUILD_URL "${f_gki_build_json}" |tr -d \")

    wget  -c "${snapshot_download_url}/${ANDROID_BUILD_CONFIG}-misc_info.txt" -O misc_info.txt

else
    echo "Unsported BUILD_TYPE specified: ${BUILD_TYPE}"
    exit 1
fi


apt-get update && apt-get install -y wget selinux-utils cpio rsync sudo bc curl git xz-utils virtualenv

export LKFT_WORK_DIR="/linaro-android/lkft"
export JOB_NAME
export BUILD_NUMBER
export BUILD_URL
export ANDROID_BUILD_CONFIG
export TUXSUITE_DOWNLOAD_URL
export SRCREV_kernel
export KERNEL_DESCRIBE
#export ENV_DRY_RUN=true

rm -fr "${LKFT_WORK_DIR}" && mkdir -p "${LKFT_WORK_DIR}"

git clone -b lkft https://android-git.linaro.org/git/android-build-configs ${LKFT_WORK_DIR}/android-build-configs

wget -c https://git.linaro.org/ci/job/configs.git/plain/lkft/lava-job-definitions/submit_for_testing-v2.sh -O "${LKFT_WORK_DIR}/submit_for_testing-v2.sh" && chmod +x "${LKFT_WORK_DIR}/submit_for_testing-v2.sh"

# prepare misc_info.txt which is required by submit_for_testing-v2.sh
for f in ${ANDROID_BUILD_CONFIG}; do
    mkdir -p ${LKFT_WORK_DIR}/out/${f}
    f_misc_info="${LKFT_WORK_DIR}/out/${f}/misc_info.txt"
    cp -v misc_info.txt "${f_misc_info}"
done

pip install virtualenv
virtualenv --python=python3  .venv
source .venv/bin/activate
pip install Jinja2 requests urllib3 ruamel.yaml squad-client

${LKFT_WORK_DIR}/submit_for_testing-v2.sh
